#!/bin/bash
pkg upgrade -y
pkg install fribidi termux-api termux-tools -y
clear
echo "Please let app have access to Storage"
termux-setup-storage
echo "Installing dependencies"
pkg install -y tor torsocks git golang
clear
echo "Let download transport sources"
echo "Please turn ON Psiphon, Lantern, HotspotShield or whatever you have"
mkdir -p storage/Documents/tor/{git,conf}
cd
cd storage/Documents/tor/git
export GOPATH=$PWD
git clone https://git.torproject.org/pluggable-transports/obfs4.git
cd obfs4/obfs4proxy
export GOBIN=$PWD
go get -v
cd
cp storage/Documents/tor/git/obfs4/obfs4proxy/obfs4proxy /data/data/com.termux/files/usr/bin/
chmod +x /data/data/com.termux/files/usr/bin/obfs4proxy
cd
cd storage/Documents/tor/git
git clone https://git.torproject.org/pluggable-transports/meek.git
cd 
cd storage/Documents/tor/git/meek/meek-client
export GOBIN=$PWD
go get -v
cp storage/Documents/tor/git/meek/meek-client/meek-client /data/data/com.termux/files/usr/bin/
chmod +x /data/data/com.termux/files/usr/bin/meekclient



